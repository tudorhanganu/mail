/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tudorhanganu.data;

import jodd.mail.Email;

/**
 *
 * @author Tudor Hanganu
 */
public class AdvancedEmail extends Email {
    private String host;
    private String userEmail;
    private String password;
    
    /**
     * Default Constructor
     */
    public AdvancedEmail(){
        this.host = "";
        this.userEmail = "";
        this.password = "";
    }
    
    /**
     * Non-default constructor
     * 
     * @param host
     * @param userEmailAddress
     * @param password
     */
    public AdvancedEmail(final String host, final String userEmail, final String password) {
        this.host = host;
        this.userEmail = userEmail;
        this.password = password;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    
    
    
}
