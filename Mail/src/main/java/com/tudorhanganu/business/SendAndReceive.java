/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tudorhanganu.business;

import com.tudorhanganu.data.AdvancedEmail;
import java.io.File;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import javax.activation.DataSource;
import javax.mail.Flags;

import jodd.mail.Email;
import jodd.mail.EmailAttachment;
import jodd.mail.EmailFilter;
import jodd.mail.EmailMessage;
import jodd.mail.ImapServer;
import jodd.mail.MailServer;
import jodd.mail.RFC2822AddressParser;
import jodd.mail.ReceiveMailSession;
import jodd.mail.ReceivedEmail;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author Tudor Hanganu
 */
public class SendAndReceive  {
    
    //Logger variable
    private final static Logger LOG = LoggerFactory.getLogger(SendAndReceive.class);

   /**
   * THis is the send mail method, which takes care of gathering information about the users to send to, the sender and the content.
   * 
   * @param mail ADvancedEmail object, holding information such as host, email address and password, this is the sender.
   * @param emailContent text message of the email.
   * @param emailSubject subject of the email.
   * @param messageHTML embedded HTML message.
   * @param receiveEmail recipient email address array.
   * @param cc array of email addresses.
   * @param bcc array of email addresses.
   * @param attachment array of strings, holding names for files.
   * @param embeddedAttachment array of strings, holding names for files.
   * 
   * 
   * @return Email object, from JODD.
   * 
   */
    public Email sendEmail(AdvancedEmail mail, String emailContent, String emailSubject, String messageHTML, String[] receiveEmail,  String[] cc, String[] bcc, String[] attachment, String[] embeddedAttachment){
        //checking the state of the email adresses
        //iterating through all adresses in the email arrays
        
        int confirmation = 1;
        
        for (int iterator = 0; iterator < receiveEmail.length; iterator++){
            if (!checkEmail(receiveEmail[iterator])){
                confirmation = 0;
            }
        }
        
        if (checkEmail(mail.getUserEmail()) && confirmation == 1) {
            // Create SMTP server object
            SmtpServer smtpServer = MailServer.create()
                    .ssl(true)
                    .host(mail.getHost())
                    .auth(mail.getUserEmail(), mail.getPassword())
                    .buildSmtpMailServer();

            // Using the fluent style of coding create a plain text message
            Email email = Email.create()
                    .from(mail.getUserEmail())
                    .to(receiveEmail)
                    .subject(emailSubject)
                    .textMessage(emailContent);
            
                    //checking if cc, bcc, embedded attachments and attachments are null or no (if arrays are of size 0 and if strings are not equal to "")
                    if (messageHTML != ""){
                        email.htmlMessage(messageHTML);
                    }
                    if (cc.length != 0){
                        email.cc(cc);
                        //LOG.info("CC INFO: " + cc[0]);
                    }
                    if (bcc.length != 0){
                        email.bcc(bcc);
                    }
                    if (attachment.length > 0){
                        for (int iterator = 0; iterator < attachment.length; iterator++) {
                            email.attachment(EmailAttachment.with().content(attachment[iterator]));
                        }   
                    }
                    if (embeddedAttachment.length > 0){
                        for (int iterator = 0; iterator < embeddedAttachment.length; iterator++){
                            email.embeddedAttachment(EmailAttachment.with().content(new File(embeddedAttachment[iterator])));
                        }
                    }
                    
            // Like a file we open the session, send the message and close the
            // session
            try ( // A session is the object responsible for communicating with the server
                     SendMailSession session = smtpServer.createSession()) {
                // closing the session and sending off the email
                session.open();
                session.sendMail(email);
                LOG.info("Email sent");
                
                return email;
            }
        } 
         else {
            LOG.info("Unable to send email because either send or recieve addresses are invalid");
        }
        
        return new Email();
    }
    
    /**
   * THis is the send mail method, which takes care of gathering information about the users to send to, the sender and the content.
   * 
   * @param receiveMail Holds information about the email address of the person we wish to retrieve emails from.
   * 
   * @return ReceivedEmail array, holding all the unread emails from that same account.
   * 
   */
    public ReceivedEmail[] receiveEmail(AdvancedEmail receiveMail) {

        if (checkEmail(receiveMail.getUserEmail())) {
            ImapServer imapServer = MailServer.create()
                    .host(receiveMail.getHost())
                    .ssl(true)
                    .auth(receiveMail.getUserEmail(), receiveMail.getPassword())
                    //.debugMode(true)
                    .buildImapMailServer();

            try ( ReceiveMailSession session = imapServer.createSession()) {
                session.open();
                LOG.info("Message count: " + session.getMessageCount());
                ReceivedEmail[] emails = session.receiveEmailAndMarkSeen(EmailFilter.filter().flag(Flags.Flag.SEEN, false));
                if (emails != null) {
                    LOG.info("\n >>>> ReceivedEmail count = " + emails.length);
                    for (ReceivedEmail email : emails) {
                        LOG.info("\n\n===[" + email.messageNumber() + "]===");

                        // common info
                        LOG.info("FROM:" + email.from());
                        // Handling array in email object
                        LOG.info("TO:" + Arrays.toString(email.to()));
                        LOG.info("CC:" + Arrays.toString(email.cc()));
                        LOG.info("SUBJECT:" + email.subject());
                        LOG.info("PRIORITY:" + email.priority());
                        LOG.info("SENT DATE:" + email.sentDate());
                        LOG.info("RECEIVED DATE: " + email.receivedDate());

                        // process messages
                        List<EmailMessage> messages = email.messages();

                        messages.stream().map((msg) -> {
                            LOG.info("------");
                            return msg;
                        }).map((msg) -> {
                            LOG.info(msg.getEncoding());
                            return msg;
                        }).map((msg) -> {
                            LOG.info(msg.getMimeType());
                            return msg;
                        }).forEachOrdered((msg) -> {
                            LOG.info(msg.getContent());
                        });

                        // process attachments
                        List<EmailAttachment<? extends DataSource>> attachments = email.attachments();
                        if (attachments != null) {
                            LOG.info("+++++");
                            attachments.stream().map((attachment) -> {
                                LOG.info("name: " + attachment.getName());
                                return attachment;
                            }).map((attachment) -> {
                                LOG.info("cid: " + attachment.getContentId());
                                return attachment;
                            }).map((attachment) -> {
                                LOG.info("size: " + attachment.getSize());
                                return attachment;
                            }).forEachOrdered((attachment) -> {
                                attachment.writeToFile(
                                        new File("c:\\temp", attachment.getName()));
                            });
                        }
                    }
                }
                
                return emails;
            }
        } else {
            LOG.info("Unable to send email because either send or recieve addresses are invalid");
        }
        
        return new ReceivedEmail[0];
        
    }
    
     /**
   * THis is the send mail method, which takes care of gathering information about the users to send to, the sender and the content.
   * 
   * @param address email address to check
   * 
   * @return boolean, wether the email addresss is valid or no
   * 
   */
    private boolean checkEmail(String address) {
        return RFC2822AddressParser.STRICT.parseToEmailAddress(address) != null;
    }

    
    
}
