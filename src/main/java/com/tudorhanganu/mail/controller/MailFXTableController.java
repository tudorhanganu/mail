/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tudorhanganu.mail.controller;

/**
 *
 * @author Tudor Hanganu
 */


import com.tudorhanganu.application.MainApp;
import com.tudorhanganu.business.sqlOperations;
import com.tudorhanganu.business.SendAndReceive;
import com.tudorhanganu.config.ConfigBean;
import com.tudorhanganu.data.AdvancedEmail;
import com.tudorhanganu.data.FakeData;
import com.tudorhanganu.exceptions.InvalidEmailException;
import com.tudorhanganu.javafxbeans.FolderBean;
import com.tudorhanganu.javafxbeans.TableBean;
import com.tudorhanganu.properties.PropertiesManager;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;

/**
 * Sample Skeleton for 'mailFXTableView.fxml' Controller Class
 */

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import jodd.mail.ReceivedEmail;

/**
 *
 * @author Tudor Hanganu
 */
public class MailFXTableController {

    ConfigBean config = new ConfigBean();
    
    sqlOperations crud;
    
    PropertiesManager propMan = new PropertiesManager();
    
    SendAndReceive sAndR = new SendAndReceive();
    
    @FXML
    private Button refreshBtn;
    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="mailFXTableLayout"
    private AnchorPane mailFXTableLayout; // Value injected by FXMLLoader

    @FXML // fx:id="mailFXTableView"
    private TableView<AdvancedEmail> mailFXTableView; // Value injected by FXMLLoader

    @FXML // fx:id="fromColumn"
    private TableColumn<AdvancedEmail, String> fromColumn; // Value injected by FXMLLoader

    @FXML // fx:id="subjectColumn"
    private TableColumn<AdvancedEmail, String> subjectColumn; // Value injected by FXMLLoader

    @FXML // fx:id="dateColumn"
    private TableColumn<AdvancedEmail, String> dateColumn; // Value injected by FXMLLoader

    RootLayoutSplitController controller;
    
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() throws IOException {
        propMan.load(config, "", "config");
        
        crud = new sqlOperations(config);
        
        fromColumn.setCellValueFactory(cellData -> cellData.getValue().getFromField());
        subjectColumn.setCellValueFactory(cellData -> cellData.getValue().getSubjectField());
        dateColumn.setCellValueFactory(cellData -> cellData.getValue().getDateField());

        adjustColumnWidths();
        
        mailFXTableView
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(
                        (observable, oldValue, newValue) -> {
            try {
                showMailData(newValue);
                mailFXTableLayout.setFocusTraversable(false);
            } catch (InvalidEmailException ex) {
                Logger.getLogger(MailFXTableController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(MailFXTableController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
    }
    
    /**
     * Sets the root controller in the class
     * 
     * @param rootLayout
     */
    public void setController(RootLayoutSplitController rootLayout){
        this.controller = rootLayout;
    }
    
    /**
     * The table displays the emails in inbox, base one.
     *
     */
    public void displayTheTable() {
        ObservableList<AdvancedEmail> beans = crud.getAllEmails(2);
        
        mailFXTableView.setItems(beans);
    }
    
     /**
     * Sets width of each column
     *
     */
    private void adjustColumnWidths() {
        // Get the current width of the table
        double width = mailFXTableView.getPrefWidth();
        // Set width of each column
        
        fromColumn.setPrefWidth(width * 0.3);
        subjectColumn.setPrefWidth(width * 0.4);
        dateColumn.setPrefWidth(width * 0.1);
        
    }
    
    private void showMailData(AdvancedEmail email) throws InvalidEmailException, IOException {
        if (email != null) {
            if (email.getFolderID() != 4){
            controller.returnHTMlController().setFocusOff();
        }
        else {
            controller.returnHTMlController().setFocusOn();
        }
        
            AdvancedEmail emailToSend = crud.getEmailWithID(email.getEmailID());
            controller.returnHTMlController().displayEmailInfo(emailToSend);
        }
    }

    /**
     * set config of config object
     * 
     * @param config
     */
    public void setConfig(ConfigBean config){
        this.config = config;
    }

    /**
     *  display appropriate emails based on folder selected
     * 
     * @param folderID
     */
    public void displayFolderEmails(int folderID){
        ObservableList<AdvancedEmail> emails = crud.getAllEmails(folderID);
        
        mailFXTableView.setItems(emails);
    }

    /**
     * sets disabled on
     */
    public void setFocusOff(){
        mailFXTableLayout.setDisable(true);
        
    }
    
    /**
     * sets disabled off
     */
    public void setFocusOn(){
        mailFXTableLayout.setDisable(false);
    }

    @FXML
    void refresh(ActionEvent event) throws IOException {
        ArrayList<ReceivedEmail> received = sAndR.receiveEmail(config);
        
        if (received != null){
            for (ReceivedEmail rec : received){
                crud.addEmail(crud.convertEmail(rec));
            }
        }
        
        this.displayTheTable();
    }

    @FXML
    void onDragDetected(MouseEvent event){
        
        int pick = mailFXTableView.getSelectionModel().getSelectedItem().getEmailID();
        int folderID = mailFXTableView.getSelectionModel().getSelectedItem().getFolderID();
        
        System.out.println("DRAG");
        
        // checking if the email is in sent or draft boxes, cannot be dragged out of
        if (folderID != 1 && folderID != 4){
            Dragboard board = mailFXTableView.startDragAndDrop(TransferMode.ANY);
            ClipboardContent content = new ClipboardContent();
            content.putString(Integer.toString(pick));
            board.setContent(content);
        }
        
        event.consume();
    }
    
    @FXML
    void onDragDone(DragEvent event){
        
        Dragboard board = event.getDragboard();
        
        if (event.isAccepted()){
            AdvancedEmail email = mailFXTableView.getSelectionModel().getSelectedItem();
            mailFXTableView.getItems().remove(email);
        }
    }
}


