/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tudorhanganu.javafxbeans;

import java.util.Date;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Tudor Hanganu
 */
public class TableBean {
    
    private IntegerProperty id;
    private StringProperty fromField;
    private StringProperty subjectField;
    private StringProperty receivedDateField;

    /**
     * Default Coonstructor
     */
    public TableBean(){}
    
    /**
     * Main constructor
     * 
     * @param id
     * @param fromField
     * @param subjectField
     * @param receivedDateField
     */
    public TableBean(int id, String fromField, String subjectField, String receivedDateField) {
        this.id = new SimpleIntegerProperty(id);
        this.fromField = new SimpleStringProperty(fromField);
        this.subjectField = new SimpleStringProperty(subjectField);
        this.receivedDateField = new SimpleStringProperty(receivedDateField);
    }

    public StringProperty getFromField() {
        return fromField;
    }

    public StringProperty getSubjectField() {
        return subjectField;
    }

    public StringProperty getReceivedDateField() {
        return receivedDateField;
    }

    public void setFromField(String fromField) {
        this.fromField = new SimpleStringProperty(fromField);
    }

    public void setSubjectField(String subjectField) {
        this.subjectField = new SimpleStringProperty(subjectField);
    }

    public void setReceivedDateField(String receivedDateField) {
        this.receivedDateField = new SimpleStringProperty(receivedDateField);
    }
    
    
    
    
    
}
