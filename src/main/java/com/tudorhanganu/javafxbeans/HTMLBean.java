/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tudorhanganu.javafxbeans;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Tudor Hanganu
 */
public class HTMLBean {
    
    private StringProperty htmlMessage;
    
    /**
     * Main constructor
     * 
     * @param htmlMessage
     */
    public HTMLBean(String htmlMessage){
        this.htmlMessage = new SimpleStringProperty(htmlMessage);
    }

    public StringProperty getHtmlMessage() {
        return htmlMessage;
    }

    public void setHtmlMessage(String htmlMessage) {
        this.htmlMessage = new SimpleStringProperty(htmlMessage);
    }
    
    
    
}
