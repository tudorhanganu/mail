/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tudorhanganu.javafxbeans;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Tudor Hanganu
 */
public class FormBean {
    
    private StringProperty toField;
    private StringProperty ccField;
    private StringProperty bccField;
    private StringProperty subjectField;
    
    /**
     * Default Constructor
     */
    public FormBean(){}
    
    /**
     *  Main Constructor
     * 
     * @param to
     * @param cc
     * @param bcc
     * @param subject
     */
    public FormBean(String to, String cc, String bcc, String subject){
        this.toField = new SimpleStringProperty(to);
        this.ccField = new SimpleStringProperty(cc);
        this.bccField = new SimpleStringProperty(bcc);
        this.subjectField = new SimpleStringProperty(subject);
    }
    
    public StringProperty getToField(){
        return toField;
    }
    
    public StringProperty getCcField(){
        return ccField;
    }
    
    public StringProperty getBccField(){
        return bccField;
    }
    
    public StringProperty getSubjectField(){
        return subjectField;
    }
    
    public void setToField(String to){
        this.toField = new SimpleStringProperty(to);
    }
    
    public void setCcField(String cc){
        this.ccField = new SimpleStringProperty(cc);
    }
    
    public void setBccField(String bcc){
        this.bccField = new SimpleStringProperty(bcc);
    }
    
    public void setSubjectField(String subject){
        this.subjectField = new SimpleStringProperty(subject);
    }
    
}
