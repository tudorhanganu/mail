/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tudorhanganu.application;

import com.tudorhanganu.config.ConfigBean;
import com.tudorhanganu.mail.controller.MailFXTreeController;
import com.tudorhanganu.mail.controller.PropertiesController;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.tudorhanganu.properties.PropertiesManager;

/**
 *
 * @author Tudor Hanganu
 */
public class MainApp extends Application {
    
    private final static Logger LOG = LoggerFactory.getLogger(MainApp.class);

    public static Stage primaryStage;

    public Parent rootLayout;
    private Parent rootPane;
    
    private Locale currentLocale;
    
    private PropertiesManager propMan;
    private ConfigBean config;

    /**
     * Default constructor
     */
    public MainApp() {
        this.config = new ConfigBean();
    }

    /**
     * Start method, loads config form if does not exist, if does, launches app.
     * 
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        
        if (getConfig()){
            initRootLayout();
        }
        else {
            initPropertiesLayout();
        }
        
        this.primaryStage.setTitle(ResourceBundle.getBundle("MessageBundle").getString("Title"));
        primaryStage.show();
    }

    /**
     * Load the root layout and controller.
     */
    public void initRootLayout() {
        
        currentLocale = Locale.getDefault();
        
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(ResourceBundle.getBundle("MessageBundle")); 
            
            loader.setLocation(MainApp.class.getResource("/fxml/rootLayout.fxml"));
            rootLayout = (AnchorPane) loader.load();
            
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            
        } catch (IOException e) {
            LOG.error("Error display table", e);
        }
    }

    /**
     * Load the properties form and controller
     */
    public void initPropertiesLayout(){
        currentLocale = Locale.getDefault();
        
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(ResourceBundle.getBundle("MessageBundle")); 
            
            loader.setLocation(MainApp.class.getResource("/fxml/propertiesFormView.fxml"));
            rootPane = (GridPane) loader.load();
            
             PropertiesController controller = loader.getController();
             
             controller.setConfig(config);
            
            Scene scene = new Scene(rootPane);
            primaryStage.setScene(scene);
            
        } catch (IOException e) {
            LOG.error("Error display table", e);
        }
    }
    
    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
        System.exit(0);
    }
    
    /**
     *  Returns boolean if it can load the config.properties file
     * 
     * @return boolean -> the result
     * @throws IOException
     */
    public boolean getConfig() throws IOException{
        propMan = new PropertiesManager();
        config = new ConfigBean();
        if (propMan.load(config, "", "config")){
            return true;
        }
        return false;
        
    }
}
