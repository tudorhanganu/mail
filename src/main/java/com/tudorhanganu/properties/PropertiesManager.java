/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tudorhanganu.properties;

import com.tudorhanganu.config.ConfigBean;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import static java.nio.file.Files.newInputStream;
import static java.nio.file.Files.newOutputStream;
import java.nio.file.Path;
import static java.nio.file.Paths.get;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Tudor Hanganu
 */
public class PropertiesManager {
    
    private final static Logger LOG = LoggerFactory.getLogger(PropertiesManager.class);
    
    /**
     * loads information into a configBean
     * 
     * @param config
     * @param path
     * @param fileName
     * @return
     * @throws IOException
     */
    public final boolean load(final ConfigBean config, final String path, final String fileName) throws IOException{
        
        boolean exists = false;
        Properties prop = new Properties();

        Path txtFile = get(path, fileName + ".properties");
        
        if (Files.exists(txtFile)) {
            try ( InputStream propFileStream = newInputStream(txtFile);) {
                prop.load(propFileStream);
            }
            
            config.setUsername(prop.getProperty("userName"));
            config.setEmailAddress(prop.getProperty("emailAddress"));
            config.setPassword(prop.getProperty("mailPassword"));
            config.setImapURL(prop.getProperty("imapURL"));
            config.setSmtpURL(prop.getProperty("smtpURL"));
            config.setImapPort(prop.getProperty("imapPort"));
            config.setSmtpPort(prop.getProperty("smtpPort"));
            config.setSqlURL(prop.getProperty("mysqlURL"));
            config.setDbName(prop.getProperty("mysqlDatabase"));
            config.setDbPort(prop.getProperty("mysqlPort"));
            config.setDbUsername(prop.getProperty("mysqlUser"));
            config.setDbPassword(prop.getProperty("mysqlPassword"));

            exists = true;
        }
        return exists;
    }
    
    /**
     * saves information into a properties file from a configBean
     * 
     * @param config
     * @param path
     * @param fileName
     * @throws IOException
     */
    public final void save(final ConfigBean config, final String path, final String fileName) throws IOException{
        
        Properties prop = new Properties();

        prop.setProperty("userName", config.username());
        prop.setProperty("emailAddress", config.email());
        prop.setProperty("mailPassword", config.password());
        prop.setProperty("imapURL", config.imapUrl());
        prop.setProperty("smtpURL", config.smtpUrl());
        prop.setProperty("imapPort", config.imapPort());
        prop.setProperty("smtpPort", config.smtpPort());
        prop.setProperty("mysqlURL", config.sqlurl());
        prop.setProperty("mysqlDatabase", config.dbname());
        prop.setProperty("mysqlPort", config.sqlPort());
        prop.setProperty("mysqlUser", config.dbuser());
        prop.setProperty("mysqlPassword", config.dbpass());

        Path txtFile = get(path, fileName + ".properties");

        // Creates the file or if file exists it is truncated to length of zero
        // before writing
        try ( OutputStream propFileStream = newOutputStream(txtFile)) {
            prop.store(propFileStream, "SMTP Properties");
        }
        
    }
    
}
