/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tudorhanganu.data;

import com.tudorhanganu.javafxbeans.FolderBean;
import com.tudorhanganu.javafxbeans.TableBean;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * USELESS CLASS !!!!!!!!
 * 
 *
 * @author Tudor Hanganu
 */
public class FakeData {
    
    /**
     *
     * Fake data of folder beans
     * 
     * @return list of beans
     */
    public ObservableList<FolderBean> getAllFolderBean(){
        
        ObservableList<FolderBean> beans = FXCollections.observableArrayList();
        
        FolderBean bean_one = new FolderBean(1, "Inbox");
        FolderBean bean_two = new FolderBean(2, "Sent");
        FolderBean bean_three = new FolderBean(3, "Draft");
        FolderBean bean_four = new FolderBean(4, "Garbage");
        FolderBean bean_five = new FolderBean(5, "Work");
        
        beans.add(bean_one);
        beans.add(bean_two);
        beans.add(bean_three);
        beans.add(bean_four);
        beans.add(bean_five);
        
        return beans;
        
    }
    
    /**
     *  FAKE DATA OF TABLE beanas
     * 
     * @return list of fake data table beans
     */
    public ObservableList<TableBean> getAllTableBean(){
        
        ObservableList<TableBean> beans = FXCollections.observableArrayList();
        
        TableBean tb1 = new TableBean(1,"tudorhanganu@live.ca", "This is a sample email", "2020-02-09");
        TableBean tb2 = new TableBean(2,"randomemail@gmail.com", "Dinosaurs are cool", "2020-01-11");
        TableBean tb3 = new TableBean(3,"netbeansofficial@net.com", "Yo yo yo", "2019-01-01");
        
        beans.add(tb1);
        beans.add(tb2);
        beans.add(tb3);
        
        return beans;
    }
    
}
