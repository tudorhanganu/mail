/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tudorhanganu.test;

import com.tudorhanganu.business.SendAndReceive;
import com.tudorhanganu.config.ConfigBean;
import com.tudorhanganu.exceptions.InvalidEmailException;
import java.io.File;
import java.util.ArrayList;
import jodd.mail.Email;
import jodd.mail.ReceivedEmail;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Tudor Hanganu
 */
public class SendAndReceiveTest {
    
    private final Logger log = LoggerFactory.getLogger(getClass().getName());
    @Ignore
    @Test
    public void testSendMail() throws InvalidEmailException{
        log.info("regular METHOD");
        ConfigBean configSend = new ConfigBean("smtp.gmail.com", "tudorsendmail@gmail.com", "nanapapa123");
        ConfigBean configReceive = new ConfigBean("imap.gmail.com", "tudorreceivemail@gmail.com", "nanapapa123");
        
        SendAndReceive sendAndReceive = new SendAndReceive();
        
        String subject = "Test Email - 30/09";
        String message = "Hello Tudor, this is a test email";
        String[] to = {"tudorreceivemail@gmail.com"};
        
        Email sendMail = sendAndReceive.sendEmail(configSend, to, subject, message, "", new String[0], new String[0], new ArrayList<File>(), new ArrayList<File>());
        
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            log.error("Threaded sleep failed", e);
        }
        
        ArrayList<ReceivedEmail> mailBeansReceive = sendAndReceive.receiveEmail(configReceive);
        System.out.println("REGULAR RECEIVED " + mailBeansReceive.size());
        ReceivedEmail receivedMail = mailBeansReceive.get(0);
        
        boolean status = false;
        
        String indice = null;
        
        if (sendMail.to()[0].toString().equals(receivedMail.to()[0].toString())){
            if (sendMail.from().toString().equals(receivedMail.from().toString())){
                if (sendMail.subject().equals(receivedMail.subject())){
                    if ((sendMail.messages().get(0).getContent().trim()).equalsIgnoreCase(receivedMail.messages().get(0).getContent().trim())){
                        status = true;
                    }
                    else {
                        indice = "Messages";
                        
                        log.info(sendMail.messages().get(0).getContent());
                        log.info(receivedMail.messages().get(0).getContent());
                    }
                }
                else {
                    indice = "Subjects";
                }
            } 
            else {
                indice = "From Addresses";
            }
        }
        else {
            indice = "To Addresses";
        }
        
        assertTrue(indice + " were not the same", status);
    }
    @Ignore
    @Test
    public void testSendMailWithCc() throws InvalidEmailException {
        log.info("CC METHOD");
        ConfigBean configSend = new ConfigBean("smtp.gmail.com", "tudorsendmail@gmail.com", "nanapapa123");
        ConfigBean configReceive = new ConfigBean("imap.gmail.com", "tudorcc0@gmail.com", "nanapapa123");
        
        SendAndReceive sendAndReceive = new SendAndReceive();
        
        String subject = "Test Email - 30/09";
        String message = "Hello Tudor, this is a test email with a CC";
        String[] to = {"tudor@gmail.com"};
        String[] cc = {"tudorcc0@gmail.com"};
        
        Email sendMail = sendAndReceive.sendEmail(configSend, to, subject, message, "", cc, new String[0], new ArrayList<File>(), new ArrayList<File>());
        
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            log.error("Threaded sleep failed", e);
        }
        
        ArrayList<ReceivedEmail> mailBeansReceive = sendAndReceive.receiveEmail(configReceive);
        System.out.println("CC RECEIVED " + mailBeansReceive.size());
        ReceivedEmail receivedMail = mailBeansReceive.get(0);
        
        boolean status = false;
        
        String indice = null;
        
        if (sendMail.to()[0].toString().equals(receivedMail.to()[0].toString())){
            if (sendMail.from().toString().equals(receivedMail.from().toString())){
                if (sendMail.subject().toString().equals(receivedMail.subject().toString())){
                    if ((sendMail.messages().get(0).getContent().trim()).equalsIgnoreCase(receivedMail.messages().get(0).getContent().trim())){
                        status = true;
                    }
                    else {
                        indice = "Messages";
                        
                        log.info(sendMail.messages().get(0).getContent().toString());
                        log.info(receivedMail.messages().get(0).getContent().toString());
                    }
                }
                else {
                    indice = "Subjects";
                }
            } 
            else {
                indice = "From Addresses";
            }
        }
        else {
            indice = "To Addresses";
        }
        
        assertTrue(indice + " were not the same", status);
        
    }
    @Ignore
    @Test
    public void testSendMailWithBcc() throws InvalidEmailException {
        log.info("BCC METHOD");
        ConfigBean configSend = new ConfigBean("smtp.gmail.com", "tudorsendmail@gmail.com", "nanapapa123");
        ConfigBean configReceive = new ConfigBean("imap.gmail.com", "tudorcc2@gmail.com", "nanapapa123");
        
        SendAndReceive sendAndReceive = new SendAndReceive();
        
        String subject = "Test Email - 30/09";
        String message = "Hello Tudor, this is a test email with a BCC";
        String[] to = {"tudor@gmail.com"};
        String[] bcc = {"tudorcc2@gmail.com"};
        
        Email sendMail = sendAndReceive.sendEmail(configSend, to, subject, message, "", new String[0], bcc, new ArrayList<File>(), new ArrayList<File>());
        
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            log.error("Threaded sleep failed", e);
        }
        
        ArrayList<ReceivedEmail> mailBeansReceive = sendAndReceive.receiveEmail(configReceive);
        System.out.println("BCC RECEIVED "+mailBeansReceive.size());
        ReceivedEmail receivedMail = mailBeansReceive.get(0);
        
        boolean status = false;
        
        String indice = null;
        
        if (sendMail.to()[0].toString().equals(receivedMail.to()[0].toString())){
            if (sendMail.from().toString().equals(receivedMail.from().toString())){
                if (sendMail.subject().toString().equals(receivedMail.subject().toString())){
                    if ((sendMail.messages().get(0).getContent().trim()).equalsIgnoreCase(receivedMail.messages().get(0).getContent().trim())){
                        status = true;
                    }
                    else {
                        indice = "Messages";
                        
                        log.info(sendMail.messages().get(0).getContent().toString());
                        log.info(receivedMail.messages().get(0).getContent().toString());
                    }
                }
                else {
                    indice = "Subjects";
                }
            } 
            else {
                indice = "From Addresses";
            }
        }
        else {
            indice = "To Addresses";
        }
        
        assertTrue(indice + " were not the same", status);
    }
    @Ignore
    @Test
    public void testSendWithHtmlMessage() throws InvalidEmailException {
        log.info("HTML METHOD");
        ConfigBean configSend = new ConfigBean("smtp.gmail.com", "tudorsendmail@gmail.com", "nanapapa123");
        ConfigBean configReceive = new ConfigBean("imap.gmail.com", "tudorreceivemail@gmail.com", "nanapapa123");
        
        SendAndReceive sendAndReceive = new SendAndReceive();
        
        String subject = "Test Email - 30/09";
        String message = "Hello Tudor, this is a test email with a an embedded HTML message";
        String[] to = {"tudorreceivemail@gmail.com"};
        
        Email sendMail = sendAndReceive.sendEmail(configSend, to, subject, message, "<p>This is an additional message</p>", new String[0], new String[0], new ArrayList<File>(), new ArrayList<File>());
        
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            log.error("Threaded sleep failed", e);
        }
        
        ArrayList<ReceivedEmail> mailBeansReceive = sendAndReceive.receiveEmail(configReceive);
        
        System.out.println("Received " + mailBeansReceive.size());
        
        ReceivedEmail receivedMail = mailBeansReceive.get(0);
        
        boolean status = false;
        
        String indice = null;
        
        if (sendMail.to()[0].toString().equals(receivedMail.to()[0].toString())){
            if (sendMail.from().toString().equals(receivedMail.from().toString())){
                if (sendMail.subject().toString().equals(receivedMail.subject().toString())){
                    if ((sendMail.messages().get(0).getContent().trim()).equalsIgnoreCase(receivedMail.messages().get(0).getContent().trim())){
                        if ((sendMail.messages().get(1).getContent().trim()).equalsIgnoreCase(receivedMail.messages().get(1).getContent().trim())){
                            status = true;
                        }
                        else {
                            indice = "HTML Messages ";
                        }
                    }
                    else {
                        indice = "Messages";
                    }
                }
                else {
                    indice = "Subjects";
                }
            } 
            else {
                indice = "From Addresses";
            }
        }
        else {
            indice = "To Addresses";
        }
        
        assertTrue(indice + " were not the same", status);
    }
    @Ignore
    @Test
    public void testSendMailWithAttachments() throws InvalidEmailException {
        ConfigBean configSend = new ConfigBean("smtp.gmail.com", "tudorsendmail@gmail.com", "nanapapa123");
        ConfigBean configReceive = new ConfigBean("imap.gmail.com", "tudorreceivemail@gmail.com", "nanapapa123");
        
        SendAndReceive sendAndReceive = new SendAndReceive();
        
        String subject = "Test Email - 30/09";
        String message = "Hello Tudor, this is a test email with attachments";
        String[] to = {"tudorreceivemail@gmail.com"};
        
        File file = new File("C:\\temp\\Gmail.png");
        ArrayList<File > regularAttachments = new ArrayList<File>();
        regularAttachments.add(file);
        ArrayList<File > embeddedAttachments = new ArrayList<File>();
        embeddedAttachments.add(file);
        
        Email sendMail = sendAndReceive.sendEmail(configSend, to, subject, message, "", new String[0], new String[0], regularAttachments, embeddedAttachments);
        
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            log.error("Threaded sleep failed", e);
        }
        
        ArrayList<ReceivedEmail> mailBeansReceive = sendAndReceive.receiveEmail(configReceive);
        
        System.out.println("Received " + mailBeansReceive.size());
        
        ReceivedEmail receivedMail = mailBeansReceive.get(0);
        
        boolean status = false;
        
        String indice = null;
        
        if (sendMail.to()[0].toString().equals(receivedMail.to()[0].toString())){
            if (sendMail.from().toString().equals(receivedMail.from().toString())){
                if (sendMail.subject().toString().equals(receivedMail.subject().toString())){
                    if ((sendMail.messages().get(0).getContent().trim()).equalsIgnoreCase(receivedMail.messages().get(0).getContent().trim())){
                        if (sendMail.attachments().get(0).getName().equals(receivedMail.attachments().get(0).getName())){
                            if (sendMail.attachments().get(1).getName().equals(receivedMail.attachments().get(1).getName())){
                                status = true;
                            }
                        }
                    } else { 
                        indice = "Messages"; 
                    }
                } else { 
                    indice = "Subjects"; 
                }
            } else { 
                indice = "From Addresses"; 
            }
        } else { 
            indice = "To Addresses"; 
        }
        
        assertTrue(indice + " were not the same", status);
        
    }
    @Ignore
    @Test (expected = InvalidEmailException.class)
    public void testSendMailWithInvalidTo() throws InvalidEmailException{
        ConfigBean configSend = new ConfigBean("smtp.gmail.com", "tudorsendmail@gmail.com", "nanapapa123");
        
        SendAndReceive sendAndReceive = new SendAndReceive();
        
        String subject = "Test Email - 30/09";
        String message = "Hello Tudor, this is a test email";
        String[] to = {"tudorreceivemail"};
        
        Email sendMail = sendAndReceive.sendEmail(configSend, to, subject, message, "", new String[0], new String[0], new ArrayList<File>(), new ArrayList<File>());
        
        fail();
   
    }
    @Ignore
    @Test (expected = InvalidEmailException.class)
    public void testSendMailWithInvalidCc() throws InvalidEmailException {
        ConfigBean configSend = new ConfigBean("smtp.gmail.com", "tudorsendmail@gmail.com", "nanapapa123");
        
        SendAndReceive sendAndReceive = new SendAndReceive();
        
        String subject = "Test Email - 30/09";
        String message = "Hello Tudor, this is a test email";
        String[] to = {"tudorreceivemail@gmail.com"};
        String[] cc = {"tudorreceive"};
        
        Email sendMail = sendAndReceive.sendEmail(configSend, to, subject, message, "", cc, new String[0], new ArrayList<File>(), new ArrayList<File>());
        
        fail();
    }
    @Ignore
    @Test (expected = InvalidEmailException.class)
    public void testSendMailWithInvalidBcc() throws InvalidEmailException {
        ConfigBean configSend = new ConfigBean("smtp.gmail.com", "tudorsendmail@gmail.com", "nanapapa123");
        
        SendAndReceive sendAndReceive = new SendAndReceive();
        
        String subject = "Test Email - 30/09";
        String message = "Hello Tudor, this is a test email";
        String[] to = {"tudorreceivemail@gmail.com"};
        String[] bcc = {"tudorreceive"};
        
        Email sendMail = sendAndReceive.sendEmail(configSend, to, subject, message, "", new String[0], bcc , new ArrayList<File>(), new ArrayList<File>());
        
        fail();
    }
    @Ignore
    @Test (expected = InvalidEmailException.class)
    public void testSendMailWithInvalidAttachment() throws InvalidEmailException {
        ConfigBean configSend = new ConfigBean("smtp.gmail.com", "tudorsendmail@gmail.com", "nanapapa123");
        
        SendAndReceive sendAndReceive = new SendAndReceive();
        
        String subject = "Test Email - 30/09";
        String message = "Hello Tudor, this is a test email with attachments";
        String[] to = {"tudorreceivemail@gmail.com"};
        
        File file = new File("C:\\temp\\NonExistantImage.png");
        ArrayList<File > regularAttachments = new ArrayList<File>();
        regularAttachments.add(file);
        
        Email sendMail = sendAndReceive.sendEmail(configSend, to, subject, message, "", new String[0], new String[0], regularAttachments, new ArrayList<File>());
        
        fail();
    }
    @Ignore
    @Test (expected = InvalidEmailException.class)
    public void testSendMailWithInvalidEmbeddedAttachment() throws InvalidEmailException {
        ConfigBean configSend = new ConfigBean("smtp.gmail.com", "tudorsendmail@gmail.com", "nanapapa123");
        
        SendAndReceive sendAndReceive = new SendAndReceive();
        
        String subject = "Test Email - 30/09";
        String message = "Hello Tudor, this is a test email with attachments";
        String[] to = {"tudorreceivemail@gmail.com"};
        
        File file = new File("C:\\temp\\NonExistantImage.png");
        ArrayList<File > embeddedAttachments = new ArrayList<File>();
        embeddedAttachments.add(file);
        
        Email sendMail = sendAndReceive.sendEmail(configSend, to, subject, message, "", new String[0], new String[0], new ArrayList<File>(), embeddedAttachments);
        
        fail();
    }
    
}
