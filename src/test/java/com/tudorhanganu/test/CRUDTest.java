/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tudorhanganu.test;

import com.tudorhanganu.business.sqlOperations;
import com.tudorhanganu.config.ConfigBean;
import com.tudorhanganu.data.AdvancedEmail;
import com.tudorhanganu.exceptions.InvalidEmailException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import javafx.collections.ObservableList;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Tudor Hanganu
 */
public class CRUDTest {
    
    private final static Logger LOG = LoggerFactory.getLogger(CRUDTest.class);
    
    ConfigBean beanconfig = new ConfigBean("jdbc:mysql://localhost:3306/mail","mail", "emailUser", "emailuser");
    sqlOperations crud = new sqlOperations(beanconfig);
    
    @Ignore
    @Test
    public void TestCreateEmail() throws InvalidEmailException, IOException{
       AdvancedEmail email = new AdvancedEmail();  
      
       email.setFolderID(2);
       email.setReceivedDate(null);
       email.setSentDate(null);
      
        email.Email.textMessage("This is an email from the test method");
        email.Email.subject("Test method");
        email.Email.to("tudorreceivemail@gmail.com");
        email.Email.from("tudorsendmail@gmail.com");
        
        crud.addEmail(email);
        
        AdvancedEmail mail = crud.getEmailWithID(21);
        
        boolean status = false;
        
        if (mail.getEmailID() == 21){
            status = true;
        }
        assertTrue("Does not exist", true);
        
    }
    @Ignore    
    @Test
    public void TestCreateFolder(){
        crud.addFoler("Temp");
        int ifExists = checkIfFolderExists("Temp");
        
        boolean status = false;
        
        if (ifExists == 5){
            status = true;
        }
         assertTrue("Does not exist", status);
    }
    @Ignore
    @Test
    public void TestDeleteEmail(){
        crud.deleteEmail(17);
        
        ObservableList<AdvancedEmail> emails = crud.getAllEmails();
        
        assertEquals("Test Find All:", 20, emails.size());
    }
    @Ignore
    @Test
    public void TestDeleteFolder(){
        crud.addFoler("Test Folder");
        int ifExists = checkIfFolderExists("Test Folder");
        
        boolean status = false;
        
        if (ifExists == 5){
            status = true;
        }
         assertTrue("Does not exist", status);
    }
    @Ignore 
    @Test
    public void TestFindAll(){
        ObservableList<AdvancedEmail> emailList = crud.getAllEmails();
        assertEquals("Test Find All:", 20, emailList.size());
    }
    @Ignore
    @Test
    public void TestFindById() throws InvalidEmailException{
        AdvancedEmail compared = crud.getEmailWithID(4);
        AdvancedEmail compareTo = new AdvancedEmail();
        compareTo.setEmailID(4);
        compareTo.setFolderID(1);
        
        compareTo.setSentDate(compared.getSentDate());
        compareTo.setReceivedDate(null);
        
        compareTo.Email.from("tudorsendmail@gmail.com");
        compareTo.Email.subject("Test Message 04");
        compareTo.Email.textMessage("Hello, this is a test email sample 04");
        compareTo.Email.htmlMessage("<p>Html Message 04</p>");
        
        if (compared.getEmailID() == compareTo.getEmailID()){
            if (compared.getFolderID() == compareTo.getFolderID()){
                if (compared.getSentDate().toString().equals(compareTo.getSentDate().toString())){
                    if (compared.getReceivedDate() == null && compareTo.getReceivedDate() == null){
                        if (compared.Email.from().getEmail().trim().equals(compareTo.Email.from().getEmail().trim())){
                            if (compared.Email.messages().get(0).getContent().trim().equals(compareTo.Email.messages().get(0).getContent().trim())){
                                if (compared.Email.messages().get(1).getContent().trim().equals(compareTo.Email.messages().get(1).getContent().trim())){   
                                    if (compared.Email.subject().trim().equals(compareTo.Email.subject().trim())){
                                        assertTrue("Not the same", true);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    @Ignore
    @Test
    public void TestUpdateEmailFolder() throws InvalidEmailException{
        crud.editEmail(1, 4);
        AdvancedEmail mail = crud.getEmailWithID(1);
        
        boolean status = false;
        
        if (mail.getFolderID() == 4){
            status = true;
        }
        
        assertTrue("Email did not change folder", status);
        
    }
    @Ignore    
    @Test
    public void TestUpdateFolderName() {
        crud.editFolder(4, "Test");
        boolean status = checkFolderName("Test");
        assertTrue("Folder Name did not change", status);
    }
    @Ignore
    @Test
    public void testAddEmail() throws IOException{
        AdvancedEmail mail = new AdvancedEmail();
        crud.addEmail(mail);
        ObservableList<AdvancedEmail> emailList = crud.getAllEmails();
        assertEquals("Test Find All:", 21, emailList.size());
    }
    
    private boolean checkFolderName(String name){
        try (Connection conn = DriverManager.getConnection(beanconfig.getSqlURL().toString(), beanconfig.getDbUsername().toString(), beanconfig.getDbPassword().toString())){
            String sqlQuery = "SELECT name FROM folder WHERE folderid = 4";
            
            Statement statement = conn.prepareStatement(sqlQuery);
            
            ResultSet result = statement.executeQuery(sqlQuery);
            
            boolean status = false;
            
            while (result.next()){
                if (result.getString(1).equals(name)){
                    status = true;
                }
            }
            
            return status;
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    private int checkIfFolderExists(String name){
        try (Connection conn = DriverManager.getConnection(beanconfig.getSqlURL().toString(), beanconfig.getDbUsername().toString(), beanconfig.getDbPassword().toString())){
            String sqlQuery = "SELECT folderid FROM folder WHERE name = ?";
            
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            statement.setString(1, name);
            
            ResultSet result = statement.executeQuery();
            
            int folderID = -1;
            
            while (result.next()){
                folderID = result.getInt(1);
            }
            
            return folderID;
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            return -1;
        }
    }
    
    @AfterClass
    public static void seedAfterTestCompleted() {
        CRUDTest c = new CRUDTest();
        c.seedDatabase();
        LOG.info("@AfterClass seeding");
        new CRUDTest().seedDatabase();
    }
    
    @Before
    public void seedDatabase() {
        LOG.info("@Before seeding");
        final String seedDataScript = loadAsString("createTables.sql");
        try (Connection connection = DriverManager.getConnection(beanconfig.sqlurl(), beanconfig.dbuser(), beanconfig.dbpass())) {
            for (String statement : splitStatements(new StringReader(seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }
    
    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream)) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }
}
