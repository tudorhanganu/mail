DROP DATABASE IF EXISTS MAIL;
CREATE DATABASE MAIL;

USE MAIL;

DROP USER IF EXISTS emailUser@localhost;
CREATE USER emailUser@'localhost' IDENTIFIED BY 'emailuser';
GRANT ALL ON MAIL.* TO emailUser@'localhost';

